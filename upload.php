<?php
session_start ();
if (isset($_SESSION['kasutajatunnus'])) {
	$target_dir = "pildid/";
	$target_file = $target_dir . basename($_FILES["fail"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	$saved_file = $target_dir . $_SESSION['kasutajatunnus'] . "." . $imageFileType;
	if(isset($_POST["submit"])) {
		$check = getimagesize($_FILES["fail"]["tmp_name"]);
		if($check !== false) {
			$uploadOk = 1;
		} else {
			$uploadOk = 0;
		}
	}
	if ($uploadOk == 1) {
		move_uploaded_file($_FILES["fail"]["tmp_name"], $saved_file);
		header("Location: index.php");
	}
}
?> 