CREATE TABLE `kasutajad` (
  `id` int(11) NOT NULL,
  `kasutajatunnus` text COLLATE utf8_bin NOT NULL,
  `nimi` text COLLATE utf8_bin NOT NULL,
  `parool` text COLLATE utf8_bin NOT NULL,
  `email` text COLLATE utf8_bin NOT NULL,
  `sugu` text COLLATE utf8_bin NOT NULL,
  `kirjeldus` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;