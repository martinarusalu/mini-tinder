<?php
session_start ();

// Mis tegevus käivitada:
switch (@$_GET ['op']) {
	case 'login' :
		logIn ();
		break;
	case 'logout' :
		logOut ();
		break;
	case 'registreeri':
		register ();
		break;
}

function openCon() {
	
	// Andmebaasi kasutaja parameetrid:
	$host = "127.0.0.1";
	$user = "root";
	$pass = "admin";
	$db = "minitinder";
	
	// Uus ühendus:
	$conn = new mysqli ( $host, $user, $pass, $db );
	
	// Kas ühendus on olemas:
	if ($conn->connect_error) {
		die ( "Ei saanud luua andmebaasi ühendust: " . $conn->connect_error );
	} else {
		
		// Tagastab ühenduse:
		return $conn;
	}
}

function logIn($kasutajatunnus = null, $parool = null) {
	
	// Kas tullakse registreerimise lehelt või logitakse sisse pealehelt:
	if (isset($_POST['kasutajatunnus']) && isset($_POST['parool'])) {
		$kasutajatunnus = $_POST['kasutajatunnus'];
		$parool = $_POST['parool'];
	}
	
	// Kas kasutajatunnus ja parool olemas:
	if (isset($kasutajatunnus) && isset($parool)) {
		// Andmebaasist kasutaja andmed:
		$conn = openCon ();
		$kasutajatunnus = mysqli_real_escape_string ( $conn, $kasutajatunnus);
		$parool = mysqli_real_escape_string ( $conn, $parool);
		$sql = "SELECT id, kasutajatunnus, nimi, email, sugu, kirjeldus ";
		$sql .= "FROM kasutajad WHERE kasutajatunnus = '$kasutajatunnus' AND parool = '$parool';";
		$result = $conn->query ( $sql );
		$andmed = $result->fetch_assoc ();
		$conn->close ();
		
		// Andmed sessionisse:
		$_SESSION ['id'] = htmlentities($andmed ['id']);
		$_SESSION ['kasutajatunnus'] = htmlentities($andmed ['kasutajatunnus']);
		$_SESSION ['nimi'] = htmlentities($andmed ['nimi']);
		$_SESSION ['email'] = htmlentities($andmed ['email']);
		$_SESSION ['sugu'] = htmlentities($andmed ['sugu']);
		$_SESSION ['kirjeldus'] = htmlentities($andmed ['kirjeldus']);
		
		// Tagasi pealehele:
		header ( 'Location: index.php' );
	}
}

function logOut() {
	session_destroy ();
	header ( 'Location: index.php' );
}

function register () {
	
	// Kas kõik väljad täidetud:
	if (isset($_POST['kasutajatunnus']) &&
			isset($_POST['parool']) &&
			isset($_POST['nimi']) &&
			isset($_POST['email']) &&
			isset($_POST['sugu']) &&
			isset($_POST['kirjeldus'])) {
	
		// Puhastab sisendandmed:
		$conn = openCon ();
		$kasutajatunnus = mysqli_real_escape_string ( $conn, $_POST ['kasutajatunnus'] );
		$parool = mysqli_real_escape_string ( $conn, $_POST ['parool'] );
		$nimi = mysqli_real_escape_string ( $conn, $_POST ['nimi'] );
		$email = mysqli_real_escape_string ( $conn, $_POST ['email'] );
		$sugu = ($_POST ['sugu'] == "mees") ? "mees" : "naine";
		$kirjeldus = mysqli_real_escape_string ( $conn, $_POST ['kirjeldus'] );
		
		// Andmebaasi päring:
		$sql = "INSERT INTO kasutajad (kasutajatunnus, parool, nimi, email, sugu, kirjeldus) ";
		$sql .= "VALUES ('$kasutajatunnus','$parool','$nimi','$email','$sugu','$kirjeldus');";
		$conn->query($sql);
		$conn->close ();
		
		// Logib loodud kasutajaga ka kohe sisse:
		logIn($kasutajatunnus, $parool);
	}
}

function getPildiNimi ($kasutajatunnus = false) {
	
	// Kas sisselogitud kasutaja pilt, või kellegi teise:
	if (!$kasutajatunnus) {
		$kasutajatunnus = @$_SESSION['kasutajatunnus'];
	}
	
	// Kas on ikka sisse logitud:
	if (isset($_SESSION['kasutajatunnus'])) {
		
		// Otsib kõik kasutajanimega failid:
		$failid = glob("pildid/".$kasutajatunnus.".*");
		
		// Moodustab massiivi {failinimi, faili muutmise aeg}
		$failid = array_combine($failid, array_map("filemtime", $failid));
		
		// Sorteerib failid vastavalt muutmise ajale:
		arsort($failid);
		
		// Tagastab failinime:
		return key($failid);
	}
}

function getRandom ($eelmine = '') {
	
	// Otsib kõik vastassoolised.
	$conn = openCon ();
	$sql = "SELECT kasutajatunnus, nimi, email, kirjeldus ";
	$sql .= "FROM kasutajad WHERE sugu <> '".$_SESSION['sugu']."' AND kasutajatunnus <> '".mysqli_real_escape_string($conn, $eelmine)."';";
	$tulemus = $conn->query($sql);
	$andmed = array();
	while ($rida = $tulemus->fetch_assoc()) {
		array_push($andmed, $rida);
	}
	$conn->close ();
	
	// Valib välja juhusliku ja tagastab selle.
	$juhuslik = rand(0,sizeof($andmed)-1);
	return $andmed[$juhuslik];
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Mini-tinder</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link href="css/stiil.css" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
	<?php if (isset($_SESSION['id'])) : ?>
		<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php">Mini-tinder</a>
			</div>
			<div class="collapse navbar-collapse" id="example-navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="index.php">Profiil</a></li>
					<li><a href="index.php?op=hinda">Hinda</a></li>
					<li><a href="index.php?op=logout">Logi välja</a></li>
				</ul>
			</div>
		</nav>
		<?php if (@$_GET['op'] == 'hinda') : ?>
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<?php $kasutaja = getRandom(@$_GET['eelmine']); ?>
				<?php if ($pildinimi = getPildiNimi($kasutaja['kasutajatunnus'])) : ?>
					<img class="img-thumbnail" src="<?php echo $pildinimi; ?>" />
				<?php endif; ?>
				<p class="lead"><?php echo htmlentities($kasutaja['kirjeldus']) ?></p>
				<p class="text-center">
					<button onclick="location.href = 'index.php?op=hinda&eelmine=<?php echo htmlentities($kasutaja['kasutajatunnus'])?>'" type="button" class="btn btn-success">Like</button>
					<button onclick="location.href = 'index.php?op=hinda&eelmine=<?php echo htmlentities($kasutaja['kasutajatunnus'])?>'" type="button" class="btn btn-danger">Nope</button>
				</p>
			</div>
		<?php else : ?>
			<div class="row">
				<div class="col-md-4">
					<h1><?php echo $_SESSION['nimi']?></h1>
					<p><strong>Kasutajatunnus: </strong><?php echo $_SESSION['kasutajatunnus']?></p>
					<p><strong>Sugu: </strong><?php echo $_SESSION['sugu']?></p>
					<p><strong>E-mail: </strong><?php echo $_SESSION['email']?></p>
					<p><strong>Kirjeldus: </strong><?php echo $_SESSION['kirjeldus']?></p>
				</div>
				<div class="col-md-4">
					<?php if ($pildinimi = getPildiNimi()) : ?>
						<img class="img-thumbnail" src="<?php echo $pildinimi; ?>" />
					<?php endif; ?>
				</div>
			</div>
			<form class="form-signin" action="upload.php" method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label for="fail">Vali profiilipilt</label>
					<input class="form-control" type="file" name="fail" id="fail">
				</div>
				<button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">Lisa pilt</button>
			</form>
		<?php endif; ?>
	<?php elseif (@$_GET['op'] == 'registreeri') : ?>
		<form class="form-signin" action="index.php?op=registreeri" method="post">
			<div class="form-group">
				<h2 class="form-signin-heading">Registreeri</h2>
				<input type="username" name="kasutajatunnus" class="form-control" placeholder="Kasutajatunnus" autofocus>
				<input type="password" name="parool" class="form-control" placeholder="Parool">
				<input type="email" name="email" class="form-control" placeholder="E-mail">
				<input type="text" name="nimi" class="form-control" placeholder="Ees- ja perekonnanimi">
				<label>
					<input type="radio" name="sugu" value="mees" checked> Mees
				</label>
				<br>
				<label>
					<input type="radio" name="sugu" value="naine"> Naine
				</label>
				<textarea class="form-control" rows="3" name="kirjeldus" placeholder="Kirjeldus"></textarea>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Registreeri</button>
		</form>
	<?php else : ?>
		<form class="form-signin" action="index.php?op=login" method="post">
			<h2 class="form-signin-heading">Logi sisse</h2>
			<input type="username" name="kasutajatunnus" class="form-control" placeholder="Kasutajatunnus" autofocus>
			<input type="password" name="parool" class="form-control" placeholder="Parool">
			<button class="btn btn-lg btn-primary btn-block" type="submit">Logi sisse</button>
			<a href="index.php?op=registreeri">Registreeri</a>
		</form>
	<?php endif; ?>
	</div>
</body>
</html>